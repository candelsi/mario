//
//  mainMario.c
//  mario
//
//  Created by KABORE D Safwane on 04/11/2021.
//

#include "main.h"



int main(int argc, char** argv)
{

    int continuer = 1;
    SDL_Event event;

    /* Initialisation simple */
    if (SDL_Init(SDL_INIT_VIDEO) != 0 )
    {
        fprintf(stdout,"ehec de l'initialisation de la SDL (%s)\n",SDL_GetError());
        return -1;
    }


    /* CrÈation de la fenÍtre */
    SDL_Window* pWindow = NULL;

    pWindow = SDL_CreateWindow("Mario by DSK and SC",SDL_WINDOWPOS_UNDEFINED,
                                                                  SDL_WINDOWPOS_UNDEFINED,
                                                                  LARGEUR_ECRAN,
                                                                  HAUTEUR_ECRAN,
                                                                  SDL_WINDOW_SHOWN);

    if(pWindow==0)
    {
        fprintf(stderr,"Erreur de creation de la fenetre: %s\n",SDL_GetError());
        return -1;
    }


    SDL_Renderer* renderer = SDL_CreateRenderer(pWindow, -1, SDL_RENDERER_ACCELERATED);

    IMG_Init(IMG_INIT_JPG);
    SDL_Surface* surfaceMenuGeneral = IMG_Load("images/accueil.png");
    SDL_Surface* surfaceMenuSokoban = IMG_Load("images/accueilSokoban.png");
    SDL_Surface* surfaceIcone = IMG_Load("images/mario_icone.png");

    SDL_Texture* textureMenuGeneral;
    SDL_Texture* textureMenuSokoban = SDL_CreateTextureFromSurface (renderer, surfaceMenuSokoban);
    if(!textureMenuSokoban) printf("erreur texture sokoban");


     if (surfaceMenuGeneral && surfaceIcone)
    {
        textureMenuGeneral = SDL_CreateTextureFromSurface (renderer, surfaceMenuGeneral);
        SDL_SetWindowIcon(pWindow, surfaceIcone);   // ajouter une icone a l'ouverture du jeu
        SDL_FreeSurface(surfaceMenuGeneral);
        SDL_FreeSurface(surfaceIcone);

        if (textureMenuGeneral!= NULL)
        {
            fprintf (stderr, "Erreur de creation de la texture\n");
        }
    }
    else
    {
        fprintf (stderr, "Erreur de creation de la surface\n");

        fprintf (stderr, "Erreur de creation de l'icone\n");


    }

    while(continuer)
    {
        SDL_RenderCopy (renderer, textureMenuGeneral, NULL, NULL);
        SDL_RenderPresent(renderer);

        SDL_WaitEvent(&event);
        switch(event.type)
        {
            case SDL_QUIT :
                printf("quitter");
                continuer = 0;
                break;
            case SDL_KEYDOWN :
                switch(event.key.keysym.sym)
                {
                    case SDLK_1:
                        printf("ouverture sokoban\n");
                        SDL_RenderCopy (renderer, textureMenuSokoban, NULL, NULL);
                        SDL_RenderPresent(renderer);
                        while(continuer)
                        {
                            SDL_PollEvent(&event);
                            printf("pull event \n");
                            switch(event.type)
                            {
                                case SDL_KEYDOWN :
                                switch(event.key.keysym.sym)
                                {
                                    case SDLK_RETURN: jouerSokoban(renderer);continuer=0;break;
                                    case SDLK_e:
                                        printf("ouverture editeur\n");
                                        editeurNiveau(renderer);
                                        break;
                                default:break;
                                }break;
                            }
                        }break;
                    case SDLK_2 :
                        printf("ouverture MarioBros\n");
                        jouerMarioBross(renderer);
                        break;
                    default:break;
                }
          }
     }

    SDL_DestroyWindow(pWindow);
    SDL_Quit();


    return 0;
}
