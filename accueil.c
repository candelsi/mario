//
//  accueil.c
//  mario
//
//  Created by KABORE D Safwane on 10/11/2021.
//

#include "accueil.h"



void jouerSokoban(SDL_Renderer* renderer)
{

//    IMG_Init(IMG_INIT_JPG);
//    SDL_Surface* surfaceMenu = IMG_Load("images/menu.png");


    printf("ouverture sokoban\n");// ___________________Initailisation_________________________

    int continuer=1, victoire=0, nbObjectifs = 0;
    SDL_Texture *mario[4], *element[6];
    SDL_Texture* marioActuel = NULL;
    SDL_Rect position={0,0,TAILLE_BLOC,TAILLE_BLOC}, positionJoueur = {5,5,TAILLE_BLOC,TAILLE_BLOC};
    SDL_Event event;
    int carte[12][12];

    //chargement des sprites
    mario[HAUT]= SDL_CreateTextureFromSurface (renderer, IMG_Load("images/mario_haut.gif"));
    mario[BAS]= SDL_CreateTextureFromSurface (renderer, IMG_Load("images/mario_bas.gif"));
    mario[GAUCHE]= SDL_CreateTextureFromSurface (renderer, IMG_Load("images/mario_gauche.gif"));
    mario[DROITE]= SDL_CreateTextureFromSurface (renderer, IMG_Load("images/mario_droite.gif"));
    marioActuel =mario[BAS];

    element[VIDE]=NULL;
    element[MUR] = SDL_CreateTextureFromSurface(renderer,IMG_Load("images/mur.jpg"));
    element[CAISSE] = SDL_CreateTextureFromSurface(renderer,IMG_Load("images/caisse.jpg"));
    element[CAISSE_OK] = SDL_CreateTextureFromSurface(renderer,IMG_Load("images/caisse_ok.jpg"));
    element[OBJECTIF] = SDL_CreateTextureFromSurface(renderer,IMG_Load("images/objectif.png"));



    SDL_SetRenderDrawColor(renderer,0,0,0,0); //fond noir

    //________________________ map test ____________________________________
    for(int y=0;y<12;y++)
    {
        for(int x=0;x<12;x++)
        {
            carte[y][x]=VIDE;
        }
    }
    carte[6][6]=CAISSE_OK;
    carte[5][6]=CAISSE_OK;
    carte[7][7]=CAISSE;
    carte[10][1]=OBJECTIF;
    carte[3][3]=MUR;
    carte[11][11]=MUR;


    //_______________________________________________________________________________



    while(continuer && victoire==0)//_______________________ Boucle de jeu _____________________
    {

        SDL_RenderClear(renderer);


        nbObjectifs = afficherMap(renderer,carte,&position,element);


        if(nbObjectifs==0) victoire=1;



        position.x=positionJoueur.x*TAILLE_BLOC;
        position.y=positionJoueur.y*TAILLE_BLOC;

        SDL_RenderCopy(renderer,marioActuel,NULL,&position);
        SDL_RenderPresent(renderer);

        SDL_PollEvent(&event);
        switch(event.type)
        {
        case SDL_QUIT :
            printf("quitter");
            continuer = 0;
            break;
        case SDL_KEYDOWN :
            switch(event.key.keysym.sym)
            {
                    // POUR CEUX QUI ON UN CLAVIER QWERTY  ET AZERTY avec les touches de directions
            case SDLK_z:
            case SDLK_UP:
            case SDLK_w:
                printf("mario monte\n");
                deplacerJoueur(carte, &positionJoueur, HAUT);
                marioActuel=mario[HAUT];
                break;
            case SDLK_s :
            case SDLK_DOWN:
                printf("mario descend\n");
                deplacerJoueur(carte, &positionJoueur, BAS);
                marioActuel=mario[BAS];
                break;
            case SDLK_q :
            case SDLK_LEFT:
            case SDLK_a:
                printf("mario gauche\n");
                deplacerJoueur(carte, &positionJoueur, GAUCHE);
                marioActuel=mario[GAUCHE];
                break;
            case SDLK_d :
            case SDLK_RIGHT:
                printf("mario droite\n");
                deplacerJoueur(carte, &positionJoueur, DROITE);
                marioActuel=mario[DROITE];
                break;
            }
        }
    }

    if (victoire)
    {
        printf("victoire\n");
    }
}


int afficherMap(SDL_Renderer* renderer,int carte[][12],SDL_Rect* position,SDL_Texture* element[])
{
    int res =0;
    for(int y=0;y<12;y++)
    {
        for(int x=0;x<12;x++)
        {
            position->x=x*TAILLE_BLOC;
            position->y=y*TAILLE_BLOC;

            if(carte[y][x]!=VIDE)
            {
                if(carte[y][x]==OBJECTIF) res++;
                SDL_RenderCopy(renderer,element[carte[y][x]],NULL,position);
            }
        }
    }
    return res;
}

void afficherMap2(SDL_Renderer* renderer,int carte[][12],SDL_Rect* position,SDL_Texture* element[])
{
    for(int y=0;y<12;y++)
    {
        for(int x=0;x<12;x++)
        {
            position->x=x*TAILLE_BLOC;
            position->y=y*TAILLE_BLOC;

            if(carte[y][x]!=VIDE)
            {
                if(carte[y][x]==OBJECTIF)
                SDL_RenderCopy(renderer,element[carte[y][x]],NULL,position);
            }
        }
    }
}

void deplacerJoueur(int carte[][12], SDL_Rect* pos, int direction)
{
    printf("deplacement joueur %d\n",direction);
    switch(direction)
            {
            case HAUT:
                printf("mario monte\n");
                if((pos->y-1<0) || (carte[pos->y-1][pos->x]==MUR)) break; //fin de carte ou mur
                else if((carte[pos->y-1][pos->x]==VIDE) || (carte[pos->y-1][pos->x]==OBJECTIF)) //vide ou objectif
                {
                    pos->y--;
                    break;
                }
                else if(((carte[pos->y-2][pos->x]==VIDE) || (carte[pos->y-2][pos->x]==OBJECTIF)) && !(pos->y-2<0)) //caisse suivi de qqch
                {
                    printf("deplacer case : case1 : %d, case2 : %d\n",(carte[pos->y-1][pos->x]), (carte[pos->y-2][pos->x]));
                    deplacerCaisse(&(carte[pos->y-1][pos->x]), &(carte[pos->y-2][pos->x]));
                    pos->y--;
                    break;
                }
                else break;


            case BAS :
                printf("mario descend\n");
                if((pos->y+1>11)||(carte[pos->y+1][pos->x]==MUR)) break;//fin de carte ou mur
                else if((carte[pos->y+1][pos->x]==VIDE) || (carte[pos->y+1][pos->x]==OBJECTIF)) //vide ou objectif
                {
                    pos->y++;
                    break;
                }
                else if(((carte[pos->y+2][pos->x]==VIDE) || (carte[pos->y+2][pos->x]==OBJECTIF)) && !(pos->y+2>11)) //caisse suivi de qqch
                {
                    printf("deplacer case : case1 : %d, case2 : %d\n",(carte[pos->y+1][pos->x]), (carte[pos->y+2][pos->x]));
                    deplacerCaisse(&(carte[pos->y+1][pos->x]), &(carte[pos->y+2][pos->x]));
                    pos->y++;
                    break;
                }
                else break;


            case GAUCHE :
                printf("mario gauche\n");
                if((pos->x-1<0)||(carte[pos->y][pos->x-1]==MUR)) break;//fin de carte ou mur
                else if((carte[pos->y][pos->x-1]==VIDE) || (carte[pos->y][pos->x-1]==OBJECTIF)) //vide ou objectif
                {
                    pos->x--;
                    break;
                }
                else if(((carte[pos->y][pos->x-2]==VIDE) || (carte[pos->y][pos->x-2]==OBJECTIF)) && !(pos->x-2<0)) //caisse suivi de qqch
                {
                    deplacerCaisse(&(carte[pos->y][pos->x-1]), &(carte[pos->y][pos->x-2]));
                    pos->x--;
                    break;
                }
                else break;


            case DROITE :
                printf("mario droite\n");
                if((pos->x+1>11)||(carte[pos->y][pos->x+1]==MUR)) break;//fin de carte ou mur
                else if((carte[pos->y][pos->x+1]==VIDE) || (carte[pos->y][pos->x+1]==OBJECTIF)) //vide ou objectif
                {
                    pos->x++;
                    break;
                }
                else if(((carte[pos->y][pos->x+2]==VIDE) || (carte[pos->y][pos->x+2]==OBJECTIF)) && !(pos->x+2>11)) //caisse suivi de vide ou objectif
                {
                    deplacerCaisse(&(carte[pos->y][pos->x+1]), &(carte[pos->y][pos->x+2])); // caisse suivi de qqch
                    pos->x++;
                    break;
                }
                else break;
            }
}

void deplacerCaisse(int *premiereCase, int *secondeCase)
{
    if ((*premiereCase == CAISSE) || (*premiereCase == CAISSE_OK))
    {
        if (*secondeCase == OBJECTIF)
            *secondeCase = CAISSE_OK;
        else
            *secondeCase = CAISSE;

        if (*premiereCase == CAISSE_OK)
            *premiereCase = OBJECTIF;
        else
            *premiereCase = VIDE;
    }
}




