//
//  accueil.h
//  mario
//
//  Created by KABORE D Safwane on 10/11/2021.
//

#ifndef accueil_h
#define accueil_h

#include <stdlib.h>
#include <stdio.h>

#include <SDL2/SDL.h>
#include "constante.h"
#include <time.h>
#include <SDL2/SDL_image.h>


void jouerSokoban(SDL_Renderer* renderer);
void afficherDriver(void);
int afficherMap(SDL_Renderer* renderer,int carte[][12],SDL_Rect* position,SDL_Texture* element[]);
void afficherMap2(SDL_Renderer* renderer,int carte[][12],SDL_Rect* position,SDL_Texture* element[]);

void deplacerCaisse(int *premiereCase, int *secondeCase);
void deplacerJoueur(int carte[12][12], SDL_Rect* pos, int direction);

#endif /* accueil_h */
