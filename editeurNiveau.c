//
//  editeur.c
//  mario
//
//  Created by KABORE D Safwane on 13/11/2021.
//


#include "editeurNiveau.h"



void editeurNiveau(SDL_Renderer* renderer)
{
    SDL_Texture *element[6];
    SDL_Rect position={0,0,TAILLE_BLOC,TAILLE_BLOC};
    SDL_Event event;

    int continuer = 1, clicGaucheEnCours = 0, clicDroitEnCours = 0;
    int objetActuel = MUR, i = 0, j = 0;
    int carte[NB_BLOCS_LARGEUR][NB_BLOCS_HAUTEUR];


    element[VIDE]=NULL;
    element[MUR] = SDL_CreateTextureFromSurface(renderer,IMG_Load("images/mur.jpg"));
    element[CAISSE] = SDL_CreateTextureFromSurface(renderer,IMG_Load("images/caisse.jpg"));
    element[MARIO]    = SDL_CreateTextureFromSurface(renderer,IMG_Load("images/mario_bas.gif"));
    element[OBJECTIF] = SDL_CreateTextureFromSurface(renderer,IMG_Load("images/objectif.png"));


    if (!chargerNiveau(carte))
    {
        printf("niveau pas charger");
        exit(EXIT_FAILURE);
    }


    for (i=0;i<NB_BLOCS_LARGEUR;i++)
        for (j=0;j<NB_BLOCS_HAUTEUR;j++)
            carte[i][j]=0;



    // Boucle infinie de l'Èditeur
    while (continuer)
    {
        SDL_Delay(30);
        SDL_RenderClear(renderer);

        SDL_PollEvent(&event);
        switch(event.type)
        {
            case SDL_QUIT:
                continuer = 0;
                break;
            case SDL_MOUSEBUTTONDOWN:
                if (event.button.button == SDL_BUTTON_LEFT)
                {
                    // On met l'objet actuellement choisi (mur, caisse...) ‡ l'endroit du clic
                    carte[event.button.x / TAILLE_BLOC][event.button.y / TAILLE_BLOC] = objetActuel;
                    clicGaucheEnCours = 1; // On active un boolÈen pour retenir qu'un bouton est enfoncÈ
                    printf("objet ajouté\n");
                }
                else if (event.button.button == SDL_BUTTON_RIGHT) // Le clic droit sert ‡ effacer
                {
                    carte[event.button.x / TAILLE_BLOC][event.button.y /TAILLE_BLOC] = VIDE;
                    clicDroitEnCours = 1;
                    printf("objet elever\n");

                }
                break;
            case SDL_MOUSEBUTTONUP: // On dÈsactive le boolÈen qui disait qu'un bouton Ètait enfoncÈ
                if (event.button.button == SDL_BUTTON_LEFT)
                    clicGaucheEnCours = 0;
                else if (event.button.button == SDL_BUTTON_RIGHT)
                    clicDroitEnCours = 0;
                break;
            case SDL_MOUSEMOTION:
                if (clicGaucheEnCours) // Si on dÈplace la souris et que le bouton gauche de la souris est enfoncÈ
                {
                    carte[event.motion.x / TAILLE_BLOC][event.motion.y / TAILLE_BLOC] = objetActuel;
                }
                else if (clicDroitEnCours) // Pareil pour le bouton droit de la souris
                {
                    carte[event.motion.x / TAILLE_BLOC][event.motion.y / TAILLE_BLOC] = VIDE;
                }
                break;
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym)
                {
                    case SDLK_ESCAPE:
                        continuer = 0;
                        break;
                    case SDLK_s:
                        sauvegarderNiveau(carte);
                        printf("niveau sauvegarder\n");
                        break;
                    case SDLK_c:
                        chargerNiveau(carte);
                        printf("niveau charger\n");
                        break;
                    case SDLK_1:
                        objetActuel = MUR;
                        printf("mur\n");
                        break;
                    case SDLK_2:
                        objetActuel = CAISSE;
                        printf("caisse\n");
                        break;
                    case SDLK_3:
                        objetActuel = OBJECTIF;
                        printf("objectif\n");
                        break;
                    case SDLK_4:
                        objetActuel = MARIO;
                        printf("mario\n");
                        break;
                }
                break;
        }

        // Effacement de l'Ècran


        SDL_SetRenderDrawColor(renderer,0,0,0,0); //fond noir


        // Placement des objets ‡ l'Ècran
        for (i = 0 ; i < NB_BLOCS_LARGEUR ; i++)
        {
            for (j = 0 ; j < NB_BLOCS_HAUTEUR ; j++)
            {
                position.x = i * TAILLE_BLOC;
                position.y = j * TAILLE_BLOC;

                switch(carte[i][j])
                {
                    case MUR:
                        SDL_RenderCopy(renderer,element[MUR],NULL,&position);

                        break;
                    case CAISSE:
                        SDL_RenderCopy(renderer,element[CAISSE],NULL,&position);

                        break;
                    case OBJECTIF:
                        SDL_RenderCopy(renderer,element[OBJECTIF],NULL,&position);

                        break;
                    case MARIO:
                        SDL_RenderCopy(renderer,element[MARIO],NULL,&position);
                        break;
                }

            }

        }
        SDL_RenderPresent(renderer);
    }

}
