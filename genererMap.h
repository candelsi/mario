//
//  genererMap.h
//  mario
//
//  Created by KABORE D Safwane on 13/11/2021.
//

#ifndef genererMap_h
#define genererMap_h

#include "SDL2/SDL.h"
#include <SDL2/SDL_image.h>
#include "accueil.h"
#include "constante.h"

int chargerNiveau(int niveau[][NB_BLOCS_HAUTEUR]);
int sauvegarderNiveau(int niveau[][NB_BLOCS_HAUTEUR]);

#endif /* genererMap_h */
