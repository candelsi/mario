//
//  accueil.c
//  mario
//
//  Created by KABORE D Safwane on 10/11/2021.
//

#include "mariobross.h"



void jouerMarioBross(SDL_Renderer* renderer)
{
    //------------ Initialisation---------------
    printf("initiatilsation\n");

    int findujeu=1, sautDispo =0, test, tombe  ;
    Uint8 *touche = SDL_GetKeyboardState(&test);
    Uint32 now =0 , lastTime =0, tempsPasse=0;
    SDL_Texture* marioActuel = SDL_CreateTextureFromSurface(renderer, IMG_Load("images/mario_bas.gif"));
    SDL_Texture* sol = SDL_CreateTextureFromSurface(renderer,IMG_Load("images/murbros.png"));
    SDL_Texture* tube = SDL_CreateTextureFromSurface(renderer,IMG_Load("images/tube.png"));
    SDL_Texture* defaite = SDL_CreateTextureFromSurface(renderer,IMG_Load("images/defaite.png"));
    SDL_Texture* mur = SDL_CreateTextureFromSurface(renderer,IMG_Load("images/murVerticale.png"));
    SDL_Texture* lave = SDL_CreateTextureFromSurface(renderer,IMG_Load("images/lave.png"));
    SDL_Texture* ciel = SDL_CreateTextureFromSurface(renderer,IMG_Load("images/ciel.png"));
    SDL_Texture* poteau = SDL_CreateTextureFromSurface(renderer,IMG_Load("images/poteau.png"));
    SDL_Texture* drapeau = SDL_CreateTextureFromSurface(renderer,IMG_Load("images/drapeauBF.png"));





    SDL_Rect positionJoueur = {325,450,30,50}, redimhero = {7,0,20,34};
    Element map[NB_ELEMENT];
    SDL_Event event;

    Mario hero;
    hero.vitesseX=0;
    hero.vitesseY=0;
    hero.pos = &positionJoueur;
    hero.texture = marioActuel;

    map[0].pos=malloc(sizeof(SDL_Rect));
    map[0].pos->x = -825;
    map[0].pos->y = 0;
    map[0].pos->w = 1200;
    map[0].pos->h = 560;
    map[0].texture = ciel;
    map[0].type = BACKGROUND;
    map[1].pos=malloc(sizeof(SDL_Rect));
    map[1].pos->x = 375;
    map[1].pos->y = 0;
    map[1].pos->w = 1200;
    map[1].pos->h = 560;
    map[1].texture = ciel;
    map[1].type = BACKGROUND;
    map[2].pos=malloc(sizeof(SDL_Rect));
    map[2].pos->x = 1575;
    map[2].pos->y = 0;
    map[2].pos->w = 1200;
    map[2].pos->h = 560;
    map[2].texture = ciel;
    map[2].type = BACKGROUND;
    map[3].pos=malloc(sizeof(SDL_Rect));
    map[3].pos->x = -825;
    map[3].pos->y = 550;
    map[3].pos->w = 800;
    map[3].pos->h = 50;
    map[3].texture = lave;
    map[3].type = BACKGROUND;
    map[4].pos=malloc(sizeof(SDL_Rect));
    map[4].pos->x = -25;
    map[4].pos->y = 550;
    map[4].pos->w = 650;
    map[4].pos->h = 55;
    map[4].texture = sol;
    map[4].type = FOREGROUND;
    map[5].pos=malloc(sizeof(SDL_Rect));
    map[5].pos->x = 625;
    map[5].pos->y = 450;
    map[5].pos->w = 55;
    map[5].pos->h = 55;
    map[5].texture = mur;
    map[5].type = FOREGROUND;
    map[6].pos=malloc(sizeof(SDL_Rect));
    map[6].pos->x = 625;
    map[6].pos->y = 550;
    map[6].pos->w = 800;
    map[6].pos->h = 50;
    map[6].texture = lave;
    map[6].type = BACKGROUND;
    map[7].pos=malloc(sizeof(SDL_Rect));
    map[7].pos->x = 825;
    map[7].pos->y = 500;
    map[7].pos->w = 55;
    map[7].pos->h = 55;
    map[7].texture = mur;
    map[7].type = FOREGROUND;
    map[8].pos=malloc(sizeof(SDL_Rect));
    map[8].pos->x = 1000;
    map[8].pos->y = 400;
    map[8].pos->w = 55;
    map[8].pos->h = 55;
    map[8].texture = mur;
    map[8].type = FOREGROUND;
    map[9].pos=malloc(sizeof(SDL_Rect));
    map[9].pos->x = 1300;
    map[9].pos->y = 550;
    map[9].pos->w = 55;
    map[9].pos->h = 55;
    map[9].texture = mur;
    map[9].type = FOREGROUND;
    map[10].pos=malloc(sizeof(SDL_Rect));
    map[10].pos->x = 1425;
    map[10].pos->y = 550;
    map[10].pos->w = 650;
    map[10].pos->h = 55;
    map[10].texture = sol;
    map[10].type = FOREGROUND;
    map[11].pos=malloc(sizeof(SDL_Rect));
    map[11].pos->x = 2075;
    map[11].pos->y = 550;
    map[11].pos->w = 650;
    map[11].pos->h = 55;
    map[11].texture = lave;
    map[11].type = BACKGROUND;
    map[12].pos=malloc(sizeof(SDL_Rect));
    map[12].pos->x = 1800;
    map[12].pos->y = 260;
    map[12].pos->w = 50;
    map[12].pos->h = 300;
    map[12].texture = poteau;
    map[12].type = BACKGROUND;
    map[13].pos=malloc(sizeof(SDL_Rect));
    map[13].pos->x = 1815;
    map[13].pos->y = 268;
    map[13].pos->w = 75;
    map[13].pos->h = 55;
    map[13].texture = drapeau;
    map[13].type = BACKGROUND;






    //------------------------------------------
    printf("affichage");



    SDL_SetRenderDrawColor(renderer,0,0,0,0); //fond noir



    while(findujeu)
    {
        while(SDL_PollEvent(&event)!=0)
        {
            if(event.type == SDL_QUIT ) exit(0);
        }


        SDL_RenderClear(renderer);

        now = SDL_GetTicks();
        tempsPasse = now - lastTime;
        lastTime = now;


        //printf("nb touche : %d \n",test);

        //printf("temps passe %d \n",tempsPasse);


        if (touche[SDL_SCANCODE_Z]||touche[SDL_SCANCODE_W])
        {
            printf("mario saute\n");
            if (sautDispo)
            {
                hero.vitesseY=-15;
                sautDispo=0;
            }
        }

        if (touche[SDL_SCANCODE_Q]||touche[SDL_SCANCODE_A])
        {
            hero.vitesseX=5;
        }else if (touche[SDL_SCANCODE_D])
        {
            hero.vitesseX=-5;
        }else hero.vitesseX=0;


        for(int i=0;i<NB_ELEMENT;i++)
        {
            if (map[i].texture!=NULL) SDL_RenderCopy(renderer,map[i].texture,NULL,map[i].pos); //affichage �l�ment de map

            //printf("can go left : %d pour element %d\n",canGoLeft(hero, map[i]),i);
            if ((!canGoLeft(hero, map[i])) && hero.vitesseX>0 && map[i].type == FOREGROUND) hero.vitesseX=0; //gestion des collisions en X
            if ((!canGoRight(hero, map[i])) && hero.vitesseX<0 && map[i].type == FOREGROUND) hero.vitesseX=0;

        }

        SDL_RenderCopy(renderer,hero.texture,&redimhero,hero.pos);

        SDL_RenderPresent(renderer);



        hero.pos->y += hero.vitesseY;// peut etre
        tombe = 1 ;
        for(int i=0;i<NB_ELEMENT;i++)
        {
            map[i].pos->x += hero.vitesseX; //deplacer tous les �l�ments de la map e, fonction de la vitesse de mario


            if(isOnFloor(hero,map[i])) //
            {
                hero.pos->y = map[i].pos->y - hero.pos->h;
                hero.vitesseY = 0;
                sautDispo = 1;
                tombe = 0;
            }

            if(headIsAtHeight(hero,map[i])&& isAbove(hero, map[i])&&(hero.vitesseY<0) && map[i].type==FOREGROUND) hero.vitesseY = 0;



        }
        if(tombe&&(hero.vitesseY<15)) hero.vitesseY+=1;


        //printf("x : %d \ny : %d \nvitesseY : %d\n",hero.pos->x,hero.pos->y, hero.vitesseY);


        if(hero.pos->y>600)
        {
            findujeu = 0;
        }

        printf("marioX : %d\n",hero.pos->x);
        if(map[12].pos->x <325)
        {
            victoire(renderer, hero, map, &redimhero);
            return;
        }

        SDL_Delay(30);
    }



    SDL_RenderClear(renderer);
    SDL_RenderCopy(renderer,defaite,NULL,NULL);
    SDL_RenderPresent(renderer);

    SDL_Delay(2500);


    //free()

    return;



}

void victoire(SDL_Renderer* renderer, Mario hero, Element *map, SDL_Rect *redimhero)
{
    while(map[13].pos->y < 500)
    {
        if(hero.pos->y<500) hero.pos->y +=4;
        map[13].pos->y +=4;

        SDL_RenderClear(renderer);

        for(int i=0;i<NB_ELEMENT;i++)
        {
            if (map[i].texture!=NULL) SDL_RenderCopy(renderer,map[i].texture,NULL,map[i].pos);
        }

        SDL_RenderCopy(renderer,hero.texture,redimhero,hero.pos);
        SDL_RenderPresent(renderer);
        SDL_Delay(30);

    }
    SDL_Delay(2000);
    return;
}

int isOnFloor(Mario hero, Element obstacle)
{
    return isAbove(hero, obstacle) && footIsAtHeight(hero, obstacle) && obstacle.type == FOREGROUND;
}

int isAbove(Mario hero, Element obstacle)
{
    return ((hero.pos->x > obstacle.pos->x)&&(hero.pos->x < obstacle.pos->x + obstacle.pos->w))
    || ((hero.pos->x + hero.pos->w > obstacle.pos->x)&&(hero.pos->x + hero.pos->w < obstacle.pos->x + obstacle.pos->w));
}

int footIsAtHeight(Mario hero, Element obstacle)
{
    return (hero.pos->y + hero.pos->h > obstacle.pos->y)&&(hero.pos->y + hero.pos->h < obstacle.pos->y + obstacle.pos->h);
}

int headIsAtHeight(Mario hero, Element obstacle)
{
    return (hero.pos->y > obstacle.pos->y)&&(hero.pos->y < obstacle.pos->y + obstacle.pos->h);
}

int canGoLeft(Mario hero, Element obstacle)
{
    return !((abs(obstacle.pos->x + obstacle.pos->w - hero.pos->x) < 5) && (footIsAtHeight(hero, obstacle)||headIsAtHeight(hero, obstacle)));;
}

int canGoRight(Mario hero, Element obstacle)
{
    return !((abs(obstacle.pos->x - (hero.pos->x + hero.pos->w)) < 5) && (footIsAtHeight(hero, obstacle)||headIsAtHeight(hero, obstacle)));
}


