//
//  constante.h
//  mario
//
//  Created by KABORE D Safwane on 10/11/2021.
//

#ifndef constante_h
#define constante_h

#ifndef CONSTANTE_H_INCLUDED
#define CONSTANTE_H_INCLUDED

#define TAILLE_BLOC         50 // Taille d'un bloc (carrÈ) en pixels
#define NB_BLOCS_LARGEUR    12
#define NB_BLOCS_HAUTEUR    12
#define LARGEUR_ECRAN     TAILLE_BLOC * NB_BLOCS_LARGEUR
#define HAUTEUR_ECRAN     TAILLE_BLOC * NB_BLOCS_HAUTEUR

    enum {VIDE, MUR, CAISSE, OBJECTIF, MARIO, CAISSE_OK};
    enum {HAUT, BAS, GAUCHE, DROITE};
    enum {BACKGROUND,FOREGROUND};


#endif // CONSTANTE_H_INCLUDED
#endif /* constante_h */
