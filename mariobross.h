#ifndef MARIOBROSS_H_INCLUDED
#define MARIOBROSS_H_INCLUDED

#include <stdlib.h>
#include <stdio.h>
#include <SDL2/SDL.h>
#include "constante.h"
#include <SDL2/SDL_image.h>

#define NB_ELEMENT 14


typedef struct
{
    SDL_Rect* pos;
    SDL_Texture* texture;
    int type;
}Element;

typedef struct
{
    SDL_Rect* pos;
    int vitesseX;
    int vitesseY;
    SDL_Texture* texture;
}Mario;


void jouerMarioBross(SDL_Renderer* renderer);
void victoire(SDL_Renderer *renderer, Mario hero, Element *map, SDL_Rect *redimhero);
int isOnFloor(Mario hero, Element obstacle);
int isAbove(Mario hero, Element obstacle);
int footIsAtHeight(Mario hero, Element obstacle);
int headIsAtHeight(Mario hero, Element obstacle);
int canGoLeft(Mario hero, Element obstacle);
int canGoRight(Mario hero, Element obstacle);



#endif // MARIOBROSS_H_INCLUDED
