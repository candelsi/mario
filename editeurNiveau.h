//
//  editeur.h
//  mario
//
//  Created by KABORE D Safwane on 13/11/2021.
//

#ifndef editeurNiveau_h
#define editeurNiveau_h

#include "SDL2/SDL.h"
#include <SDL2/SDL_image.h>
#include "accueil.h"
#include "constante.h"
#include "genererMap.h"

void editeurNiveau(SDL_Renderer* renderer);
#endif /* editeurNiveau_h */
